//[SECTION] Dependencies and Modules
const express = require("express")
const ProductController = require("../controllers/productControllers")
const Auth = require('../auth.js');

//[SECTION] Routing Component
const router = express.Router();
const { AUTHENTICATE, AUTHORIZE} = Auth;
const multer = require('multer')
const path = require('path')

//[SECTION] ROUTES - POST
	//------------------Insert a Product

		const storage = multer.diskStorage({
	    destination: function (req, file, callback) {
	        callback(null, path.join(__dirname, '../images/'))
	    },
	    filename: function(request, file, callback){
	        callback(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
	    }
	})

		const upload = multer({
		    storage: storage,
		    fileFilter: (req, file, cb) => {
		        if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
		            cb(null, true);
		        } else {
		            cb(null, false);
		            return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
		        }
		    }
		})

		router.post('/create', AUTHENTICATE, AUTHORIZE, upload.single('image'), (req, res) => {
				const url = req.protocol + '://' + req.get('host')
				 ProductController.INSERTPRODUCT(req.body, req.file, url).then(resultFromController => res.send(resultFromController))});

//[SECTION] Routes- PUT
	//------------------ Update a Product Information
		router.put('/editProduct/:productId', AUTHENTICATE, AUTHORIZE, upload.single('image'), (req, res) => {
			const url = req.protocol + '://' + req.get('host')
			ProductController.UPDATEPRODUCT(req.params.productId, req.body, req.file, url).then(resultFromController => res.send(resultFromController))});


//[SECTION] ROUTES - GET
	//------------------ Retrieve all Products
		router.get("/allproducts", (req, res) => {
			ProductController.GETALLPRODUCTS().then(
				resultFromController => 
				res.send(resultFromController))
		});

	//------------------ Retrieve a single Product
	router.get('/findproduct/:productId', (req, res) => {
		console.log(req.params.productId)
		//we can retrieve the course ID by accessing the request's "params" property which contains all the paramteres provided via the URL
		ProductController.GETONEPRODUCT(req.params.productId).then(result => res.send(result));
	})
	//------------------ Retrieve all ACTIVE Products
		router.get("/activeproducts", (req, res) => {
			ProductController.GETACTIVEPRODUCTS().then(
				resultFromController => 
				res.send(resultFromController))
		});
	//------------------ Retrieve all ARCHIVED Products
		router.get("/archivedproducts", AUTHENTICATE, AUTHORIZE, (req, res) => {
			ProductController.GETARCHIVEDPRODUCTS().then(
				resultFromController => 
				res.send(resultFromController))
		});



	//------------------ Archive a Product
		router.put('/archive/:archiveProductId', AUTHENTICATE, AUTHORIZE, (req, res) => {
			ProductController.ARCHIVEPRODUCT(req.params.archiveProductId, req.body).then(
				resultFromController => res.send(resultFromController))
	});

	//------------------ Activate a Product
		router.put('/activate/:activateProductId', AUTHENTICATE, AUTHORIZE, (req, res) => {
			ProductController.ACTIVATEPRODUCT(req.params.activateProductId, req.body).then(
				resultFromController => res.send(resultFromController))
	});
	//------------------ Apply discount to a Product
		router.put('/applydiscount/:ProductId', AUTHENTICATE, AUTHORIZE, (req, res) => {
			ProductController.APPLYDISCOUNT(req.params.ProductId, req.body).then(
				resultFromController => res.send(resultFromController))
	});

//[SECTION] Routes- DEL
//[SECTION] Expose Route System
module.exports = router;


