//[SECTION] Dependencies and Modules
  const mongoose = require('mongoose');

//[SECTION] Schema/Blueprint
  const productSchema = new mongoose.Schema({
  		name: {
  			type: String,
        unique: true,
  			required: [true, 'Product Name is Required']
  		},
  		description: {
  			type: String,
        unique: true,
  			required: [true, 'Description is Required']
  		},
  		price: {
  			type: Number,
  			required: [true, 'Product Price is Required']
  		},
      image: {
        type: String,
        required: [true, 'Product Image is Required']
      },
  		isActive: {
  			type: Boolean,
  			default: true
  		}, 
  		createdOn: {
  			type: Date,
  			default: Date.now
  		}
  });

//[SECTION] Model
	module.exports = mongoose.model('Product', productSchema);
