//[SECTION] Dependencies and Modules
	const express = require('express'); 
	const mongoose = require('mongoose'); 
	const dotenv = require('dotenv'); 
	const userRoutes = require('./routes/userRoute');
	const productRoutes = require('./routes/productRoute');
	const orderRoutes = require('./routes/orderRoute');
	const cors = require('cors');
	const multer = require('multer');

//[SECTION] Environment Setup
    dotenv.config(); 
    let mongoDBString = process.env.CREDENTIALS;
	const port = process.env.PORT;

//[SECTION] Server Setup
	const app = express();
	app.use(cors());
	app.use(express.json());
	app.use(express.static(__dirname))

//[SECTION] Database Connection 
	mongoose.connect(mongoDBString);
	const connectStatus = mongoose.connection; 
	connectStatus.once('open', () => console.log(`MongoDB Database Connected`));

//[SECTION] Backend Routes 
	app.use('/users', userRoutes);
	app.use('/products', productRoutes);
	app.use('/orders', orderRoutes);


//[SECTION] Server Gateway Response
	app.get('/', (req, res) => {
		res.send(`🆆🅴🅻🅲🅾🅼🅴 🆃🅾 🅾🅽🅴-🅲🆄🅿-🅿🅾-🅲🅷🅸🅽🅾`);
	}); 
	app.listen(port, () => {
		console.log(`E-Commerce is Hosted at port :${port}`);
	});

























