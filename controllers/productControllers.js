//[SECTION] Dependencies and Modules
  const Product = require('../models/Product'); 

//[SECTION] Environment Variables Setup



//[SECTION] Functionalities [CREATE]
		//INSERT A PRODUCT
				module.exports.INSERTPRODUCT = (productData, reqFile, url) => {

			    let newProduct = new Product({
			      name: productData.name,
			      description: productData.description,
			      price: productData.price,
			      isActive : productData.isActive,
			      image: url + "/images/" + reqFile.filename
			    }); 

					return newProduct.save().then((product, err) => {
						if (product) {
							return product; 	
						} else {
							return err;
						}; 
					}).catch(error => error)
				};

//[SECTION] Functionalities [UPDATE]
		//UPDATE A PRODUCT INFORMATION
		module.exports.UPDATEPRODUCT = (productId, productData, reqFile, url) => {
			let updatedProduct = {
				name: productData.name,
				description: productData.description,
				price: productData.price,
				image: url + "/images/" + reqFile.filename
			}

			//findByIdAndUpdate(document Id, updatesTobeApplied)
			return Product.findByIdAndUpdate(productId, updatedProduct).then((product, error) => {
				if(error){
					return false;
				} else {
					return updatedProduct;
				}
			}).catch(error => error)
		}
		
//[SECTION] Functionalities [RETRIEVE]
		//RETRIEVE ALL PRODUCTS
				module.exports.GETALLPRODUCTS = () => {
					return Product.find({}).then(result => {
						return result;
					}).catch(error => error)
				};

		//RETRIEVE A SINGLE PRODUCT
		module.exports.GETONEPRODUCT = (reqParams) => {
			return Product.findById(reqParams).then(result => {
				return result
			}).catch(error => error)
		}
		//RETRIEVE ACTIVE PRODUCTS
				module.exports.GETACTIVEPRODUCTS = () => {
					return Product.find({ isActive: true}).then((found,notFound) => {
						if(found) {
							return found;
						} else {
							return ({Message: "No Active Products."});
						};
					}).catch(error => error)
				}
		//ARCHIVE A PRODUCT
		module.exports.ARCHIVEPRODUCT = (productId, productData) => {
			let archiveProduct = {
				isActive: false
			}
			return Product.findByIdAndUpdate(productId, archiveProduct).then((product, error) => {
				if(error){
					return false;
				} else {
					return true;
				}
			}).catch(error => error)
		}
		//RETRIEVE ALL ARCHIVED PRODUCTS
		module.exports.GETARCHIVEDPRODUCTS = () => {
			return Product.find({ isActive: false}).then((found,notFound) => {
				if(found) {
					return found;
				} else {
					return ({Message: "No Archived Products."});
				};
			}).catch(error => error)
		}

		//ACTIVATE A PRODUCT
		module.exports.ACTIVATEPRODUCT = (productId, productData) => {
			let activateProduct = {
				isActive: true
			}
			return Product.findByIdAndUpdate(productId, activateProduct).then((product, error) => {
				if(error){
					return false;
				} else {
					return true;
				}
			}).catch(error => error)
		}
		//RETRIEVE A SINGLE PRODUCT
		module.exports.GETONEPRODUCT = (getOneProduct) => {
			return Product.findById(getOneProduct).then((found,notFound) => {
					if(found) {
						return found;
					} else {
						return ({Message:"Product does not exist."});
					};
				}).catch(error => error)
		}

		module.exports.APPLYDISCOUNT = (productId, productData) => {

			return Product.findById(productId).then((found,notFound) => {
				if(found) {
					if(found.isActive === false){
						return ({Message: "Product is currently inactive. Activate the Product first."});
					} else {
						let applyDiscount = {
							price: (productData.Percentage_Discount / 100) * found.price
						}
						return Product.findByIdAndUpdate(productId,applyDiscount).then((product, error) =>{
							if(error){
									return ({Error_Message: "Failed to Apply DISCOUNT."});
							} else {
									return product;
							}}).catch(error => error)
					}
				} else {
						return ({Message:"Product does not exist."});
				}})
		}

//[SECTION] Functionalities [DELETE]


